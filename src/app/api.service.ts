import { Injectable } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  user: any = {};

  constructor(public google: GooglePlus, public http: HttpClient, public httpclient: HttpClientModule, public router: Router) { }

  signin() {
    this.google.login({})
      .then(res => {
        this.user = res;
        console.log(res);
        this.getData();
        this.router.navigate(['/login']);
      })
      .catch(err => {
        console.error(err);
        alert("Invalid login");
      });
  }
  getData() {
    this.http.get('https://www.googleapis.com/oauth2/v3/userinfo?access_token='+this.user.accessToken).subscribe((res:any) => {
      this.user.name = res.name;
      this.user.email = res.email;
      this.user.photo = res.photoURL;
    })
  }
}
